import { Router } from "express"
import { readFile } from 'node:fs/promises'
import { answerQuestionEnd, answerQuestionStart, showHomepage, showQuestionEnd, showQuestionStart, startQuizz } from "../controllers/quizz.js"

const router = Router()

router.get("/homepage", showHomepage)
router.post("/start",startQuizz )

router.get("/:id/start", showQuestionStart )
router.post("/:id/start", answerQuestionStart)

router.get("/:id/end", showQuestionEnd)
router.post("/:id/end", answerQuestionEnd)

export { router }