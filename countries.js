import { readFile } from 'node:fs/promises'

export const transformCountries = (rawData) => {
    return rawData.map((c) => {
        return {
            flag : c.flags.png,
            capital : c.capital[0],
            country : c.name.common
        }
    }).filter((c)=> !!c.flag && !!c.capital && !!c.country)
}

export const loadCountries = async () => {
    let rawCountries = await readFile('countries.json')
    rawCountries = JSON.parse(rawCountries)
    return transformCountries(rawCountries);
}