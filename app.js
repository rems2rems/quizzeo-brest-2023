import express from "express"

export const createApp = () => {
    const app = express()
    app.use(express.static("public"))
    app.use(express.urlencoded({ extended: false }))

    app.set("view engine", "ejs")
    app.set("views", "views")
    return app
}