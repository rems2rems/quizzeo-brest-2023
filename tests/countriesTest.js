import { loadCountries, transformCountries } from "../countries.js";
import { expect } from "chai";

describe('countries loader Test case', () => {
    it('Should create countries from raw countries data', async () => {
        let rawCountry = {
            flags: {
                png: 'https://flagcdn.com/w320/mh.png',
                svg: 'https://flagcdn.com/mh.svg',
                alt: 'The flag of Marshall Islands has...'
            },
            name: {
                common: 'Marshall Islands',
                official: 'Republic of the Marshall Islands',
                nativeName: { eng: 'marshall', mah: 'marshall' }
            },
            capital: ['Majuro']
        }
        const customCountry = transformCountries([rawCountry])[0];
        expect(customCountry).to.be.an("Object")
        expect(customCountry.country).to.be.ok
        expect(customCountry.country).to.eql("Marshall Islands")
        expect(customCountry.capital).to.be.ok
        expect(customCountry.capital).to.eql("Majuro")
        expect(customCountry.flag).to.be.ok
        expect(customCountry.flag).to.eql("https://flagcdn.com/w320/mh.png")
    })

    it('Should load countries from json file', async () => {
        const countries = await loadCountries()
        for (const country of countries) {
            expect(country).to.be.an("Object")
            expect(country.country).to.be.ok
            expect(country.capital).to.be.ok
            expect(country.flag).to.be.ok
        }
    })
});