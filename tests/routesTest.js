import { expect } from "chai";
import request from "supertest"
import { createApp } from "../app.js";
import { router as quizzRouter } from "../routes/quizz.js"
import { JSDOM } from "jsdom"

describe('[routesTest] Test case', () => {
    
    it('Should show homepage', (done) => {
        const app = createApp()
        app.use("/quizz", quizzRouter)
        request(app)
            .get('/quizz/homepage')
            .expect('Content-Type', /html/)
            .expect(200)
            .end(function (err, res) {
                if (err) throw err;
                const dom = new JSDOM(res.text);
                const startButton = dom.window.document.querySelector("button")
                // console.log(startButton.textContent); // "Hello world"
                expect(startButton).to.be.ok
                expect(startButton.textContent).to.eql("Démarrer un quizz!")
                done()
            });
    });
});