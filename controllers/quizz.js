import { loadCountries } from "../countries.js"
import { createQuizz } from "../quizzGenerator.js"

export const showHomepage = (req, res) => {
    res.render("index", { name: req.params.name })
}

export const startQuizz = async (req, res) => {
    //TODO: create quizz + show question 1
    const countries = await loadCountries()
    const quizz = createQuizz(countries)
    quizz.id = "7638-9714-7654-8762"
    res.redirect("/quizz/" + quizz.id + "/start")
}

export const showQuestionStart = (req, res) => {
    res.render("showQuestionStart", { desiredInfo: "la capitale", tipType: "country", tip: "France", id: req.params.id })
}

export const answerQuestionStart = (req, res) => {
    //TODO : duo/square/cash
    res.redirect("/quizz/" + req.params.id + "/end")
}

export const showQuestionEnd = (req, res) => {

    res.render("showQuestionEnd", { desiredInfo: "la capitale", tipType: "country", tip: "France", id: req.params.id })
}

export const answerQuestionEnd = (req, res) => {
    //TODO : answer question + move to next question, or show results page
    res.redirect("/quizz/" + req.params.id + "/start")
}